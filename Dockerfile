From python:3.8-slim-buster
ENV PYTHONUNBUFFERED 1
WORKDIR /app
COPY ./requirements.txt /app/
COPY static ./static
COPY manage.py ./manage.py
RUN pip install -r requirements.txt
COPY . /app

